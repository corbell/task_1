import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vk_task.settings')

app = Celery('vk_profile')
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()