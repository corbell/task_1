from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .tasks import get_groups

# Create your views here.


class MyProfile(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request: Request):
        user_auth = request.user.social_auth.first()
        print(request.user.social_auth.first().access_token)
        print(request.user.social_auth.first().id)
        token = user_auth.extra_data['access_token']
        id = user_auth.extra_data['id']
        return get_groups.delay(token, id).get()
        # return Response(data='my Profile')
