from django.urls import path, include

from .views import MyProfile

my_profile = MyProfile.as_view()

urlpatterns = [
    path('me/', my_profile, name='my-profile'),
]