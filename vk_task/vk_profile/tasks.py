import requests
from rest_framework.response import Response

from celery.decorators import task
# GET_GROUPS = 'https://api.vk.com/method/groups.get?user_id=553925763&extended=1&access_token=4803b7887375e4ab07bb59c157e223e2055187561dd634e1cd60638feb2edb3c189e01508c04a6a6219ac&v=5.101'


METHOD = 'groups.get'


@task
def get_groups(token: str, user_id: str):

    PATH = 'https://api.vk.com/method/' + METHOD + '?user_id=' + user_id + '&extended=1&access_token=' + token + '&v=5.101'

    res = requests.get(PATH)
    print(res.text)
    return Response(data=res.text)
